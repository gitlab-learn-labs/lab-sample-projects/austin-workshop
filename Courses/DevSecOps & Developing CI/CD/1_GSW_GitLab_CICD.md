# Introduction

This Challenge will walk through the steps of creating your first GitLab CICD pipeline

# Step 01 - Creating A Simple Pipeline

1. First click **_workshop-project_** in the top left of the screen. Now that we have our project imported, go ahead and take a look at the **_.gitlab-ci.yml_** file.
2. Notice that we have a simple pipeline already defined. We have two stages, **_build & test_**, as well as an **_build_** job that uses the **_before_script , script, & after_script_** keywords.

3. In the **_test_** job we want to use the **_after_script_** keyword to echo out that the test has completed. First to edit the pipeline we need to click **Edit in pipeline editor**. Once there lets go ahead and add an **_after_script_** to the **_test_** job:

   ```plaintext
   after_script:
      - echo "Our race track has been tested!"
   ```

   Your new **_test_** job should look like this:

   ```plaintext
   test:
    stage: test
    image: gliderlabs/herokuish:latest
    script:
      - cp -R . /tmp/app
      - /bin/herokuish buildpack test
    after_script:
      - echo "Our race track has been tested!"
   ```

# Step 02 - Parallel Jobs

1. Right now our jobs will run sequentially, but what if we wanted the 2 jobs to run parallel? to do this we are going to use the _needs_ keyword__

2. Right now we only have the **_test_** job running during the test stage, so let's add the **_super_fast_test_** job below the **_test_** job.:

   ```plaintext
    super_fast_test:
      stage: test
      script:
        - echo "If your not first your last"
        - return 0
      needs: []
   ```
3. Now that we have the two jobs we also want to modify the execution order so that they run at the same time. Add the following line to the end of the **_test_** code block:

   ```plaintext
   needs: []
   ```

   The new **_test_** should look like this:

   ```plaintext
    test:
      stage: test
      image: gliderlabs/herokuish:latest
      script:
        - cp -R . /tmp/app
        - /bin/herokuish buildpack test
      after_script:
        - echo "Our race track has been tested!"
      needs: []
   ```

# Step 03 - Rules & Failure

1. What if our **_super_fast_test_** job had been failing? Lets add a new line to the **_script_** to make it fail:

   ```plaintext
   - exit 1
   ```
2. What if we also wanted to allow failure on a rule that we had set? Let's test that out on the **_code_quality_** job. **Change** the rules to be the below code:

   ```plaintext
   rules:
       - if: $CI_COMMIT_BRANCH == 'main'
         allow_failure: true
   ```
3. Your yaml for the **_code_quality & unit_test_** job should look like this:

   ```plaintext
    test:
      stage: test
      image: gliderlabs/herokuish:latest
      script:
        - cp -R . /tmp/app
        - /bin/herokuish buildpack test
      after_script:
        - echo "Our race track has been tested!"
      needs: []
      rules:
        - if: $CI_COMMIT_BRANCH == 'main'

    super_fast_test:
      stage: test
      script:
        - echo "If your not first your last"
        - return 1
      needs: []
      rules:
        - if: $CI_COMMIT_BRANCH == 'main'
        allow_failure: true
   ```
4. Lets go ahead and click **Commit Changes** and use the left hand menu to click through **CI/CD -\> Pipelines**, then click the hyperlink from the most recently kicked off pipeline that starts with **_#_**. In the pipeline view as the jobs run click into each of them to see how our added **_rules & allow_failure_** have changed the output. We should also see that the code_quality job fails with a warning but the pipeline succeeds. Note that if you use the completed **_rules & allow_failure_** the target branch for the rule will change.

> If you run into any issues you can use the left hand navigation menu to click through **CI/CD -\> Pipelines**, click **Run pipeline**, select **_rules-and-failures_** and click **Run pipeline** once again.
